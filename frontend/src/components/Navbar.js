import { useRouter } from "next/router";
import Link from "next/link";
import { FiClipboard, FiPlus, FiPower, FiUser } from "react-icons/fi";

import {
  Container,
  Option,
  Text,
  ImageLogo,
} from "../styles/components/Navbar";
import IF from "../assets/IF.svg";
import { useAuth } from "../hooks/auth";
import { useToast } from "../hooks/toast";
import { useCallback, useState, useEffect } from "react";

const Navbar = () => {
  const history = useRouter();
  const { signOut } = useAuth();
  const { addToast } = useToast();
  const { user } = useAuth();
  const [userProfessor, setUserProfessor] = useState("");

  const handleSignOut = useCallback(() => {
    history.push("/");
    signOut();
    addToast({
      type: "success",
      title: "Deslogado com sucesso",
    });
  }, [signOut, addToast]);

  useEffect(async () => {
    if (user.type == "Professor") {
      setUserProfessor(true);
    }
  }, []);

  return (
    <Container>
    
        <ImageLogo>
          <IF />
        </ImageLogo>
      
      <Link href="/user">
        <Option>
          <FiUser size="1.8em" strokeWidth="0.833333" />
          <Text>{user && user.name ? user.name.split(" ")[0] : ""}</Text>
        </Option>
      </Link>
      {
        userProfessor ?
          <Link href="/register">
            <Option>
              <FiPlus size="1.8em" strokeWidth="0.833333" />
              <Text>Adicionar</Text>
            </Option>
          </Link>
          : ''
      }
      {
        userProfessor ?
          ''
          : <Link href="/home">
            <Option>
              <FiClipboard size="1.8em" strokeWidth="0.833333" />
              <Text>Eventos</Text>
            </Option>
          </Link>
      }

      <Option onClick={handleSignOut}>
        <FiPower size="1.8em" strokeWidth="0.833333" />
        <Text>Sair</Text>
      </Option>
    </Container>
  );
};

export default Navbar;
