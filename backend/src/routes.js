const express = require('express');
const userRoutes = require('./services/user/userRoutes');
const registerRoutes = require('./services/register/registerRoutes');
const eventRoutes = require('./services/event/eventRoutes');
const authRoutes = require('./services/auth/routes');
const middlewares = require('./middlewares');

const routes = new express.Router();

routes.use('/users', userRoutes);
routes.use('/registers', registerRoutes);
routes.use('/events', eventRoutes);
routes.use('/auth', authRoutes);

module.exports = routes;