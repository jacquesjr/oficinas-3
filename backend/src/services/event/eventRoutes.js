const express = require('express')
const controller = require('./eventController')

const eventRoutes = new express.Router()
//Criar um novo evento
eventRoutes.post('/', controller.create)

//Busca todos os eventos
eventRoutes.get('/', controller.findAll)

//Busca todos os eventos que o aluno participa
eventRoutes.get('/:id', controller.findByUser)

//Busca todos os eventos pelo professor que é propietário
eventRoutes.get('/owner/:id', controller.findByOwner)

//Edita um evento
eventRoutes.put('/:id', controller.update)

//remove um evento
eventRoutes.delete('/:id', controller.delete)

module.exports = eventRoutes